var rutas = require('../rutas.json');
var valores = require('../valores.json');

var gulp = require('gulp');

gulp.task('imagenes', function () {
	var changed = require('gulp-changed');

	gulp.src( rutas.origen.imagenes + "*" )
		.pipe( changed( rutas.destino.img, {
			hasChanged: changed.compareSha1Digest
		} ))
		.pipe( gulp.dest( rutas.destino.img ));
});
