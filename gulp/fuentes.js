var rutas = require('../rutas.json');
var valores = require('../valores.json');

var gulp = require('gulp');

gulp.task('fuentes', function () {
	var changed = require('gulp-changed');

	gulp.src( rutas.origen.fuentes + "*" )
		.pipe( changed( rutas.destino.fnt, {
			hasChanged: changed.compareSha1Digest
		} ))
		.pipe( gulp.dest( rutas.destino.fnt ));
});
