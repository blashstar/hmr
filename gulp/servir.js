var rutas = require('../rutas.json');
var valores = require('../valores.json');

var gulp = require('gulp');

gulp.task('servir', ["html", "css", "js", "fuentes", "imagenes"], function(){
	var browserSync = require("browser-sync");

    browserSync({
        server: rutas.destino.html,
        directory: true,
        xip: true,
        open: "external",
        logLevel: "info",
        logConnections: true,
        logFileChanges: true,
        logPrefix: "{ ATK }",
        notify: false,
        scrollProportionally: false,
        injectChanges: true,
        files: rutas.destino.html + "**/*",
        minify: false
    });

    gulp.watch( rutas.origen.twig + '**/*.twig', ['html']);
    gulp.watch( rutas.origen.less + '**/*.less', ['css']);
    gulp.watch( rutas.origen.ecmascript + '**/*.js', ['js']);
});
