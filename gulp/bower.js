var rutas = require('../rutas.json');
var valores = require('../valores.json');

var gulp = require('gulp');

gulp.task('bower.dependencias', function() {
    var bowerMain = require('bower-main');

    var archivosLess = bowerMain('less');
    var archivosCss = bowerMain('css');

    gulp.src(archivosLess.normal)
	    .pipe(gulp.dest(rutas.origen.less + "base/"))

    gulp.src(archivosCss.normal)
	    .pipe(gulp.dest(rutas.destino.css))
});