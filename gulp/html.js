var rutas = require('../rutas.json');
var valores = require('../valores.json');

var gulp = require('gulp');

gulp.task('html', function () {
	var twig = require('gulp-twig');
	var plumber = require('gulp-plumber');
	var prettify = require('gulp-prettify');
	var changed = require('gulp-changed');

	gulp.src(rutas.origen.twig + "**/*.twig")
		.pipe(plumber())
		.pipe(twig({
				data: valores,
				base: rutas.origen.twig + "base"
			}))
		.pipe(prettify({ indent_with_tabs: true}))
		.pipe( changed( rutas.destino.html, {
			hasChanged: changed.compareSha1Digest,
			extension: '.html'
		} ))
		.pipe(gulp.dest(rutas.destino.html));

});
