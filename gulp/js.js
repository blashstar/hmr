var rutas = require('../rutas.json');
var valores = require('../valores.json');

var gulp = require('gulp');

gulp.task('js', function(){
	var jspm = require('gulp-jspm-build');
	var ignore = require('gulp-ignore');
	var plumber = require('gulp-plumber');
	var xo = require('gulp-xo');

	gulp.src(rutas.origen.ecmascript + "*")
		.pipe(plumber())
		.pipe(xo());

    jspm({
		bundleOptions: {
            minify: false,
            mangle: false
        },
        bundles: [{
			src: 'aplicacion',
			dst: 'aplicacion.js' }
        ],
		bundleSfx: true
    })
    .pipe(plumber())
	.pipe(ignore.exclude("config.js"))
    .pipe(gulp.dest(rutas.destino.js));
});
