var rutas = require('../rutas.json');
var valores = require('../valores.json');

var gulp = require('gulp');

gulp.task('css', function () {
	var less = require('gulp-less');
	var less = require('gulp-less-sourcemap');
	var postcss = require('gulp-postcss');
	var plumber = require('gulp-plumber');
	var filter = require('gulp-filter');
	var changed = require('gulp-changed');


	var autoprefixer = require('autoprefixer');
	var mqpacker = require("css-mqpacker")

	var filtro = filter("*.css", {restore: true});

	gulp.src( rutas.origen.less + "*.less" )
		.pipe( plumber())
		/*.pipe( changed( rutas.destino.css, {
			hasChanged: changed.compareLastModifiedTime,
			extension: '.css'
		} ))*/
		.pipe( less({
			paths: [ rutas.origen.less + "base/" ],
			sourceMap: {
	            sourceMapRootpath: rutas.origen.less
	        }
		}))
		.pipe( filtro )
		.pipe( postcss( [
			autoprefixer({
				browsers: ['last 2 versions']
			}),
			mqpacker({
				sort: true
			})
		] ))
		.pipe( changed( rutas.destino.css, {
			hasChanged: changed.compareSha1Digest,
			extension: '.css'
		} ))
		.pipe( filtro.restore )
		.pipe( gulp.dest( rutas.destino.css ));
});
